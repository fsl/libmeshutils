include ${FSLCONFDIR}/default.mk

PROJNAME = meshUtils
XFILES   = run_mesh_utils
SOFILES  = libfsl-meshUtils.so

LIBS = -lfsl-warpfns -lfsl-basisfield -lfsl-vtkio -lfsl-MVdisc \
       -lfsl-first_lib -lfsl-shapeModel -lfsl-meshclass -lfsl-newimage \
       -lfsl-miscmaths -lfsl-cprob -lfsl-NewNifti -lfsl-znz -lfsl-utils

all: ${SOFILES} ${XFILES}

libfsl-meshUtils.so: meshUtils.o
	$(CXX) $(CXXFLAGS) -shared -o $@ $^ ${LDFLAGS}

run_mesh_utils: meshUtils.o run_mesh_utils.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}
