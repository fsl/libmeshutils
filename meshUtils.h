/*
 *  meshUtils.h
 *
 *
 *  Created by Brian Patenaude on 04/04/2008.
 *  Copyright 2008 University of Oxford All rights reserved.
 *
 */
/*  CCOPYRIGHT  */
#ifndef MESHUTILS_H
#define MESHUTILS_H

#include <string>
#include <vector>

#include "fslvtkio/fslvtkio.h"
#include "armawrap/newmat.h"
#include "meshclass/meshclass.h"
#include "newimage/newimageall.h"

namespace meshutils {

  class meshUtils: public fslvtkio::fslvtkIO {

  public:
    meshUtils();
    meshUtils(const std::string & fname, const fslvtkIO::DataType i);

    //convience method to read polydata
    void loadMesh(const std::string & meshname);

    float getMinX() { return Points.Column(1).Minimum(); }
    float getMaxX() { return Points.Column(1).Maximum(); }
    float getMinY() { return Points.Column(2).Minimum(); }
    float getMaxY() { return Points.Column(2).Maximum(); }
    float getMinZ() { return Points.Column(3).Minimum(); }
    float getMaxZ() { return Points.Column(3).Maximum(); }


    static void generateRandomMeshUsingScalar(const mesh::Mesh & m, const std::string & outname, const std::vector<bool> & scal, const int & N);
	//	static void addModesToModelUsingMask(shapemodel::shapeModel * min, const vector<bool> & scal);
	//	static void getConditionalMeanAndVariance(shapemodel::shapeModel * min, volume4D<float> & iCondMean, volume4D<float> & iCondVar , const volume<float> & im, const int & mode, const float & bmin, const float & bmax, const float & res, const float & mean_offset);
    static void generateRandom6DOFMatrices( const std::string & outname, const int & N);

    int getNumberOfPolygons(){ return Polygons.Nrows(); }

    float interpolateScalar(const unsigned int & tri_index, const float & x, const float & y, const float& z );

    static NEWMAT::ReturnMatrix vectorOfVectorsToMatrix(const std::vector< std::vector<float> > & vin);
    static void fileToVector(const std::string & fname, std::vector<std::string> list);
    static std::vector<std::string> fileToVector(const std::string & fname );

    static NEWMAT::ReturnMatrix readFlirtMat(const std::string & fname);
    static void writeFlirtMatrix(const NEWMAT::Matrix & fmat, const std::string & fname);


    void getBounds(int *bounds, const float & xdim, const float & ydim,const float & zdim);
    static void getBounds(const mesh::Mesh & m, int *bounds,const float & xdim, const float & ydim,const float & zdim) ;


    template<class Tdist,class Tim> //instantiations are in .cc file
    void SurfDistToLabels(std::vector<Tdist> & dist, const NEWIMAGE::volume<Tim> & image);
    template<class Tdist,class Tim> //instantiations are in .cc file
    void SurfDistToLabels(std::vector<Tdist> & dist, const NEWIMAGE::volume<Tim> & image, const Tim & label);

    void SurfScalarsMeanAndStdev(std::vector<std::string> meshList, NEWMAT::Matrix & MeanPoints, NEWMAT::Matrix & MeanScalars, NEWMAT::Matrix & StDevScalars );

    static void meshReg(mesh::Mesh & m, const NEWMAT::Matrix & fmat);
    void meshReg(const NEWMAT::Matrix & fmat);

    static void shift3DVertexMatrix(NEWMAT::Matrix & mat, const float & tx, const float & ty, const float & tz );
    static void shift3DVertexColumnVector(NEWMAT::ColumnVector & mat, const float & tx, const float & ty, const float & tz );
    static void shift3DMesh(mesh::Mesh & m, const float & tx, const float & ty, const float & tz );

    static NEWMAT::ReturnMatrix subSampleMatrix(const NEWMAT::Matrix & m, const std::vector<bool> & vmask );
    static NEWMAT::ReturnMatrix subSample_Nby1_3D_Matrix(const NEWMAT::Matrix & m, const std::vector<bool> & vmask );


    void shiftPoints(const float & tx, const float & ty, const float & tz );
    void scalePoints( const  float & sx, const float & sy, const float & sz );

    static NEWMAT::ReturnMatrix shiftPolygonMatrix(const NEWMAT::Matrix & mat, const int & shift );
    void shiftPolygonMatrix( const int & shift );
    static NEWMAT::ReturnMatrix meshPointsToMatrix(const mesh::Mesh & m1);
    static bool checkLine(const float & p1, const float & p2, const float & test);
    static bool checkTriangleNeighbour(const short & tri0, const short & tri1, const short & tri2 , const short & ind0, const short & ind1, short & ind0new , short & ind1new);
    static void intersectionPoint(const float & ycut, const float & px0, const float & py0, const float & pz0, const  float & dx, const float & dy, const float & dz, std::vector<float> & px, std::vector<float> & py, std::vector<float> & pz);

    template<class T,class T2>
    void deformSurface(const NEWIMAGE::volume<T> & im, const float & maxit, const float & w_im, const float & wTang, const float & maxTri, const float & w_norm, const T & max_thresh,const unsigned int & interRate, const bool & enableInteraction, const std::string & name);


    //transformation matrix utilities
    static void preMultiplyGlobalRotation(NEWMAT::Matrix & fmat, const NEWMAT::Matrix & R);
    static void preMultiplyGlobalScale(NEWMAT::Matrix & fmat, const float & s);
    static void preMultiplyGlobalScale(NEWMAT::Matrix & fmat, const float & sx,const float & sy, const float & sz);
    static void preMultiplyTranslation(NEWMAT::Matrix & fmat, const  float & tx, const float & ty, const float & tz );
    static NEWMAT::ReturnMatrix getIdentityMatrix(const short N);
    //end of transofrmation matrix utilities

    //this should output the mask values as well as the truncated mesh
    template<class T>
    void sampleImageAtPoints(const NEWIMAGE::volume<T> & immask, std::vector<T> & vsamples);

    void LQSurfaceReg(const NEWMAT::Matrix & refPoints, NEWMAT::Matrix & fmat, const int & dof);

    void combineMeshesWithVectorsAndScalars(const std::vector<std::string> & meshlist);

    //template<class T>
    void findMidPointOfMidSlice(const NEWIMAGE::volume<char> & im, const NEWMAT::Matrix & fmat, float & cx, float & cy, float & cz);
    std::vector<float> sliceMesh(const float & ycut);

    void sampleMeshProfilesFromImage(const NEWIMAGE::volume<float> & image, const float & sample_interval, const unsigned int & ipp);


    static void warpMeshWithDefField(const std::string & fieldname, const std::string & meshname, const std::string & meshoutname, const float & dx, const float & dy, const float & dz);

    template< class T >
    void warpGridWithDefField(const NEWIMAGE::volume4D<T> & fieldname, const float & dx, const float & dy, const float & dz);

    template< class T >
    static void warpGridWithDefField(const NEWIMAGE::volume4D<T> & defField, std::vector<float> & points_in, float warpSc,const float & dx, const float & dy, const float & dz);


    float drawTriangleScalars(NEWIMAGE::volume<float>& image, NEWIMAGE::volume<int> &count, const unsigned int & tri_index);



    //-----------------------VERTEX ANALYSIS STUFF-----------------------//
    //return linear transformation matrix
    NEWMAT::Matrix reg_leastsq(const NEWMAT::Matrix & TargetPoints,  const short & dof);


    static void applyReg(NEWMAT::Matrix & pts, const NEWMAT::Matrix & fmat);
    static NEWMAT::ReturnMatrix calculateRotation(const NEWMAT::Matrix & Pts_src_dm, const NEWMAT::Matrix & Pts_targ_dm);
    static NEWMAT::ReturnMatrix calculateScale(const NEWMAT::Matrix & Pts_src_dm, const NEWMAT::Matrix & Pts_targ_dm, const bool & global);

    NEWMAT::Matrix alignSurfaces(const std::string & src_list, const short & dof, const std::string & outname );
    double maxScalar();
    double meanScalar();


    //-----------------------VERTEX ANALYSIS STUFF-----------------------//



    static float myatan2(const float & y, const float & x);

    template<class T>
    static void cartesianToSphericalCoord(std::vector<T> & verts);

    template<class T>
    static void sphericalToCartesianCoord(std::vector<T> & verts);

    static NEWMAT::ReturnMatrix addSphericalCorrdinates( const NEWMAT::Matrix & m1, const NEWMAT::Matrix  & m2 );
    static NEWMAT::ReturnMatrix subtractSphericalCoordinates( const NEWMAT::Matrix & m1, const NEWMAT::Matrix  &  m2 );
    static NEWMAT::ReturnMatrix  averageSphericalCorrdinates( const NEWMAT::Matrix & m1, const NEWMAT::Matrix & m2 , int & N1, const int & N2);
    static void SVDarcSpherical( NEWMAT::Matrix & m1, NEWMAT::DiagonalMatrix & D, NEWMAT::Matrix & U, NEWMAT::Matrix & V);



    void getSphericalCoordFromCart(NEWMAT::Matrix & r, NEWMAT::Matrix & theta, NEWMAT::Matrix & phi);
    static void cartesianToSphericalCoord(NEWMAT::Matrix & verts);
    static void sphericalToCartesianCoord(NEWMAT::Matrix & verts);

    template<class T>
    static void cartesianToSphericalCoord(mesh::Mesh & m);

    template<class T>
    static void sphericalToCartesianCoord(mesh::Mesh & m);

    static void combinedSharedBoundaries(const std::string & mname1, const std::string & mname2 );
    static void labelAndCombineSharedBoundaries(const std::string & mname1, const std::string & mname2, const std::string & mout1name );
    //static void appendSharedBoundaryMask(const string & mname1, const string & mname2,const string & mbase, const string & mout1name, const bool & indexed, const bool & useSc2 );
    NEWMAT::ReturnMatrix appendSharedBoundaryMask(const NEWMAT::Matrix & Points2 );

    //this will sample the new set of points based on indices from mask then replace the original
    void sampleSharedBoundaryByMask(const NEWMAT::Matrix & Points2);

    static void removeRow(NEWMAT::Matrix & mat, int ind );
    static NEWMAT::ColumnVector sample_image_at_vertices(std::string meshname, std::string imagename);
    static void sample_image_at_verticesNN(const std::string & meshname, const std::string & imagename, const std::string & outname);
    static void sampleSumAndWrite(const std::string & meshname, const std::string & imagename, const std::string & outname);
    static void meshReg(const std::string & meshname, const std::string & fname, const std::string & outname, bool noinv );
    static void draw_segment(NEWIMAGE::volume<short>& image, const mesh::Pt& p1, const mesh::Pt& p2, int label);
    static NEWIMAGE::volume<short> draw_mesh(const NEWIMAGE::volume<short>& image, const mesh::Mesh &m, int label);
    static NEWIMAGE::volume<short> make_mask_from_mesh(const NEWIMAGE::volume<float> & image, const mesh::Mesh& m, int label, int* bounds, const bool & sep_boundary);

    static void fillMesh(const std::string & imname, const std::string & meshname, const std::string & outname, const int & label, const bool & sep_boundary  );

    static bool findVertex(const NEWMAT::Matrix & vert1,const NEWMAT::Matrix & vert2, int ind1 );

    static void applyFlirtThenSBmask(const std::string & mname1, const std::string & mname2,const std::string & mflirtname, const std::string & mout1name);
    static void do_work_uncentreMesh(const std::string & inim, const std::string & inmesh, const std::string & outmesh);
    static void do_work_MeshReg(const std::string & inim, const std::string & inmesh, const std::string & outmesh);
    static void subtractMeshes(const std::string & mesh1name, const std::string & mesh2name, const std::string & out);

    template<class T>
    static void warpMeshWithDefField(mesh::Mesh & m, const NEWIMAGE::volume4D<T> & defField, const NEWMAT::Matrix & mat);

    static NEWMAT::ReturnMatrix getDeformedVector(const NEWMAT::ColumnVector & mean, const NEWMAT::Matrix & modes, const NEWMAT::ColumnVector & eigs, const std::vector<float> & vars  );


    template<class T>
    void ugridToImage(NEWIMAGE::volume<T> & im);


  private:
    NEWIMAGE::volume<short> image;


  };
}
#endif
